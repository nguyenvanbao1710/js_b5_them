var tinhTien = function () {
  var tongTien = null;
  var nhaDan = document.getElementById("txt-NhaDan").value;
  var doanhNghiep = document.getElementById("txt-DoanhNghiep").value;
  var loaiKH = document.getElementById("txt-KhachHang").value;

  var maKH = document.getElementById("txt-MaKH").value;
  var soKenhCaoCap = document.getElementById("txt-KenhCaoCap").value * 1;
  var soKetNoi = document.getElementById("txt-KetNoi").value * 1;
  var phiThueKenhCaoCap = 50;
  var phiHoaDon = 4.5;
  var phiDichVu = 20.5;
  var phiThueKenh = 7.5;

  if (loaiKH == nhaDan) {
    tongTien = phiHoaDon + phiDichVu + phiThueKenh * soKetNoi;
  }
  if (loaiKH == doanhNghiep) {
    var phiThueKenhDN = null;
    if (soKetNoi <= 10) {
      phiThueKenhDN = 75;
    } else {
      phiThueKenhDN = (soKetNoi - 10) * 5 + 75;
    }

    tongTien =
      phiHoaDon + phiDichVu + phiThueKenhCaoCap * soKenhCaoCap + phiThueKenhDN;
  }

  document.getElementById(
    "result"
  ).innerHTML = `Mã khách hàng: ${maKH}; Tổng tiền cáp: $${tongTien.toLocaleString()}`;
};
