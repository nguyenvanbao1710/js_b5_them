var tinhThue = function () {
  var nameValue = document.getElementById("txt-HoTen").value;
  var tongThuNhapValue = document.getElementById("txt-TongThuNhap").value * 1;
  var nguoiPhuThuocValue =
    document.getElementById("txt-NguoiPhuThuoc").value * 1;

  // Thu nhap chiu thue = tong thu nhap - 4tr - so nguoi phu thuoc * 1tr6
  // 0 - 60 : 5%
  // > 60 - 120: 10%
  // > 120 - 210: 15%
  // > 210- 384: 20%
  // > 384 - 624: 25%
  // > 624 - 960: 30%
  // > 960: 35%

  var thuNhapChiuThue = tongThuNhapValue - 4e6 - nguoiPhuThuocValue * 1600000;
  var thueCanTra = null;
  if (thuNhapChiuThue <= 60e6) {
    thueCanTra = thuNhapChiuThue * 0.05;
  } else if (60e6 < thuNhapChiuThue && thuNhapChiuThue <= 120e6) {
    thueCanTra = (thuNhapChiuThue - 60e6) * 0.1 + 60e6 * 0.05;
  } else if (120e6 < thuNhapChiuThue && thuNhapChiuThue <= 210e6) {
    thueCanTra = (thuNhapChiuThue - 120e6) * 0.15 + 60e6 * 0.1 + 60e6 * 0.05;
  } else if (210e6 < thuNhapChiuThue && thuNhapChiuThue <= 384e6) {
    thueCanTra =
      (thuNhapChiuThue - 210e6) * 0.2 + 90e6 * 0.15 + 60e6 * 0.1 + 60e6 * 0.05;
  } else if (384e6 < thuNhapChiuThue && thuNhapChiuThue <= 624e6) {
    thueCanTra =
      (thuNhapChiuThue - 384e6) * 0.25 +
      174e6 * 0.2 +
      90e6 * 0.15 +
      60e6 * 0.1 +
      60e6 * 0.05;
  } else if (624e6 < thuNhapChiuThue && thuNhapChiuThue <= 960e6) {
    thueCanTra =
      (thuNhapChiuThue - 624e6) * 0.3 +
      240e6 * 0.25 +
      174e6 * 0.2 +
      90e6 * 0.15 +
      60e6 * 0.1 +
      60e6 * 0.05;
  } else {
    thueCanTra =
      (thuNhapChiuThue - 960e6) * 0.35 +
      336e6 * 0.3 +
      240e6 * 0.25 +
      174e6 * 0.2 +
      90e6 * 0.15 +
      60e6 * 0.1 +
      60e6 * 0.05;
  }

  document.getElementById(
    "result"
  ).innerHTML = `${nameValue} - Thuế cần đóng là: ${thueCanTra.toLocaleString()} VND`;
};
